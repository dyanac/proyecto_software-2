# README #

# Instalación

## Instalar Xampp

>https://www.apachefriends.org/es/download.html

## Instalar CodeIgniter

1. Descargar CodeIgniter.
> http://www.codeigniter.com/download
2. Descomprimir y copiar a.
> C:\xampp\htdocs

## Crear VirtualHosts

0. Basado en.
> http://sawmac.com/xampp/virtualhosts/
1. Abrir.
> C:\windows\system32\drivers\etc\hosts
2. Copiar al final. ('modulo.local' asi se pondra en la url )
> 127.0.0.1 modulo.local
3. Abrir.
> C:\xampp\apache\conf\extra\httpd-vhosts.conf
4. Copiar al final.
> Tomar en considaración **DocumentRoot** y **Directory ** son el mismo destino(proyecto)
>
```
#!php
NameVirtualHost *
  <VirtualHost *>
    DocumentRoot "C:\xampp\htdocs"
    ServerName localhost
  </VirtualHost>
  <VirtualHost *>
    DocumentRoot "C:\xampp\htdocs\proyecto_software-2"
    ServerName modulo.local
  <Directory "C:\xampp\htdocs\proyecto_software-2">
    Order allow,deny
    Allow from all
  </Directory>
</VirtualHost>
```
5. Reiniciar Apache.

## Instalar MongoDB(Windows)

1. Ir y descargar version mas conveniente:
> http://www.mongodb.org/downloads
2. Ejecutar descargable
>http://docs.mongodb.org/manual/tutorial/install-mongodb-on-windows/

## Instalar Driver MongoDB PHP
0. Basado en.
> http://devzone.co.in/configure-mongodb-php-windows-xampp-5-simple-steps/
> (si tienen problemas probar copiarlo dentro C:\xampp\php\ext en vez de C:\xampp\php)

## Instalar MongoDb - CodeIgniter
0. Basado en.
> https://github.com/bcit-ci/CodeIgniter/wiki/Using-MongoDB-in-Codeigniter
1. Ir y descargar como zip.
> https://github.com/intekhabrizvi/Codeigniter-mongo-library
2. Descomprimir.
3. Copiar.
> application/config/mongo_db.php a application/config/
4. Copiar.
> application/libraries/Mongo_db.php a application/libraries/


## Instalar phpUnit
0. Ir y descargar.
> https://phpunit.de/
1. Ejecutar archivo .phar, en windows tienen que ejecutarlo con php.exe(php.exe esta en xampp/php).

## Integrar CI con phpUnit
0. Basado en.
> http://www.jamesfairhurst.co.uk/posts/view/codeigniter_phpunit_and_netbeans
1. Netbeans con PHPUnit
> https://netbeans.org/kb/docs/php/phpunit.html