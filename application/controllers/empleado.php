<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Empleado extends CI_Controller {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('m_empleado');
		//$this->load->library('Jquery_pagination');//-->la estrella del equipo
    }

    public function index() {
		 // Find all members, limit by 5, order by date
			$members = $this->m_empleado->listar_empleado();
			
			while($members->hasNext()){ // While we have results
			$member = $members->getNext();// Get the next result
			$data['members'][] = array(
			'id' => $member["_id"]->__toString(),
			'tipo_documento' => $member['tipo_documento'],
			'numero' => $member['numero'],
			'apellidop' => $member['apellidop'],
			//'apellidom' => $member['apellidom'],
			'nombre' => $member['nombre'],
			'sexo' => $member['sexo'],
			'fechaNacimiento' => $member['fechaNacimiento']
			);
			}
			$this->load->helper('url');
			//$this->load->theme('assets');
			//$this->output->set_template('default');
			$this->load->view('empleado.php', $data);
			
    }
	
	public function guardar() {
		$tipo_documento = $_REQUEST['tipo_documento'];
		$numero = $_REQUEST['numdoc'];
		$apellidop = $_REQUEST['apellidop'];
		$apellidom = $_REQUEST['apellidom'];
		$nombre = $_REQUEST['nombre'];
		$sexo = $_REQUEST['sexo'];
		$fechanacimiento = $_REQUEST['fechanacimiento'];
		
		$rs = $this->m_empleado->registrar_empleado(1, $tipo_documento, $numero, $apellidop, $apellidom, $nombre, $sexo, $fechanacimiento);
		if($rs){
			$this->index();
		}
		
        //$this->load->view('empleado.html');
    }
		public function nuevo() {
		
        $this->load->view('empleado_nuevo.html');
    }
    public function modificar() {
    	$iide_empleado = $_REQUEST['iide_empleado'];
    	$members = $this->m_empleado->listar_empleado($iide_empleado);
    	$this->load->view('empleado_nuevo.html');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */