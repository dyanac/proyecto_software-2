<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Factura extends CI_Controller {
    
    function __construct() {
       parent::__construct();
       // Call the Model constructor
       $this->load->model('m_factura');
    }

    public function insertar_factura()
    {
+       $this->m_factura->registrar_factura_model($id_factura,$nro_factura,$estado,$fecha,$nombre_cliente);
    }
    public function get_factura()
   {
        $data['facturas'] = $this->m_factura->listar_factura();
        $this->load->view('factura1',$data);
    }
    public function mostrar_factura() {
                
        $this->load->view('factura');
    }
}

/* End of file welcome.php */