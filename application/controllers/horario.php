<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Horario extends CI_Controller {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('m_horario'); 
    }

    public function index() {
        $members = $this->m_horario->listar_horario();
        while($members->hasNext()){ // While we have results
            $member = $members->getNext();// Get the next result
            $data['members'][] = array(
                'id'            => $member["id"],
		'nombre'        => $member['id_empleado'],
		'fecha'         => $member['fecha'],
		'hora_inicio'   => $member['hora_inicio'],
		'hora_fin'      => $member['hora_fin']
            );
        }
        $this->load->view('horario.html',$data);
    }

}