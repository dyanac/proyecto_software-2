<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kardex extends CI_Controller {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('m_kardex');
    }

    public function index() {
        $data['kardexs'] = $this->m_kardex->listar_kardex();
        $this->load->view('kardex.html', $data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */