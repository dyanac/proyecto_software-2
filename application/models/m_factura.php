<?php
/**
 * Description of m_factura
 *
 * @author Pio
 */
class m_factura extends CI_Model{
       
    function __construct()
    {
        parent::__construct();
        //$this->mongo_db->create_collection('factura');
    }
    public function registrar_factura_model($id_factura,$nro_factura,$estado,$fecha,$nombre_cliente){
        $data = array(
                'id_factura' => $id_factura,
                'nro_factura' => $nro_factura, 
                'estado' => $estado,
                'fecha' => $fecha,
                'nombre_cliente' => $nombre_cliente);
        $this->mongo_db->insert('factura', $data);
    }
    
    public function listar_factura($id = "") {
         if(empty($id)) {
            return $this->mongo_db->find_collection('factura');
        }
        $facturas = $this->mongo_db->find_collection('factura');
        foreach ($facturas as $factura) {
            if($factura['id_factura'] == $id) {
                return $factura;
            }
        }
       
    }
    public function eliminar_factura($criterio) {
        $this->mongo_db->delete_document('factura', $critero);
    }

}

/**
* 
*/
class m_factura_detalle extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
        $this->mongo_db->create_collection("factura_detalle");
    }
    public function registrart_detalle($id_factura,$concepto_titulo,$concepto_detalle,$unidades,$precio_unidad){
        $this->mongo_db->insert('factura_detalle', $data = array('id_factura' => $id_factura, 'concepto_t' => $concepto_titulo, 
                                'concepto_d' => $concepto_detalle, 'unidades' => $unidades, 'precio' => $precio_unidad));
    }
    public function listar_factura_detalle($id = "") 
    {
        if(empty($id))
        {
            return $this->mongo_db->find_collection('factura_detalle');
        }
        $facturas_detalle = $this->mongo_db->find_collection('factura_detalle');
        foreach ($facturas_detalle as $factura_d) 
        {
            if($factura_d['id_factura'] == $id)
            {
                return $factur_d;
            }
        }
    }
    public function eliminar_factura_detalle($criterio) {
        $this->mongo_db->delete_document('factura_detalle', $critero);
    }
    public function add_detalle($det= "", $id="")
    {
        //implemet
    }
    public function delete_detalle($det= "", $id="")
    {
        //track
    }
    
}