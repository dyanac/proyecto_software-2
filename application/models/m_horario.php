<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_horario
 *
 * @author Nitkellem
 */
class m_horario extends CI_Model{
   function __construct()
    {
        parent::__construct();
        $this->mongo_db->create_collection('horario');
    }
    
    public function ingresar_horario($id, $id_empleado, $fecha, $hora_inicio, $hora_fin) {
        $this->mongo_db->insert('horario', $data = array('id' => $id, 'id_empleado'=> $id_empleado,'fecha' => $fecha, 'hora_inicio' => $hora_inicio, 'hora_fin' => $hora_fin,));
    }
    public function modificar_horario($id, $id_empleado, $fecha, $hora_inicio, $hora_fin) {
        $this->mongo_db->update('horario',$data = array('id' => $id, 'id_empleado'=> $id_empleado,'fecha' => $fecha, 'hora_inicio' => $hora_inicio, 'hora_fin' => $hora_fin,));
    
    }
    public function eliminar_horario($id) {
        
        $this->mongo_db->horario->delete($id);
    }
     public function listar_horario($id = '') {
        if(empty($id)) {
            return $this->mongo_db->find_collection('horario');
        }
        $horarios = $this->mongo_db->find_collection('horario');
        foreach ($horarios as $horario) {
            if($horarios['id'] == $id) {
                return $horario;
            }
        }
    }
    
}
