<?php
/**
 * Description of m_kardex
 *
 * @author Daniel
 */
class m_kardex extends CI_Model{
       
    function __construct()
    {
        parent::__construct();
        $this->mongo_db->create_collection('kardex');
    }
    
    public function registrar_kardex($id, $tipo_ingreso, $descripcion, $monto, $fecha) {
       // $this->mongo_db->insert('kardex', $data = array('id' => $id, 'tipo_ingreso' => $tipo_ingreso, 'descripcion' => $descripcion, 'monto' => $monto, 'fecha' => $fecha));
    }
    
    public function modificar_kardex($critero, $nuevo) {
        $this->mongo_db->update_document('kardex',$critero, $nuevo);
    }
    
    public function eliminar_kardex($criterio) {
        $this->mongo_db->delete_document('kardex', $critero);
    }

    public function listar_kardex($id = '') {
        if(empty($id)) {
            return $this->mongo_db->find_collection('kardex');
        }
        $kardexs = $this->mongo_db->find_collection('kardex');
        foreach ($kardexs as $kardex) {
            if($kardex['id'] == $id) {
                return $kardex;
            }
        }
    }
}
