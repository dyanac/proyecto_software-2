<?php
/**
 * Description of m_pago
 *
 * @author Daniel
 */
class m_pago extends CI_Model{
       
    function __construct()
    {
        parent::__construct();
        $this->mongo_db->create_collection('pago');
    }
    
    public function registrar_pago($id, $codigo_factura, $tipo_pago, $tipo_tarjeta, $monto, $fecha) {
        $this->mongo_db->insert('pago', $data = array('id' => $id, 'codigo_factura' => $codigo_factura, 'tipo_pago' => $tipo_pago, 'tipo_tarjeta' => $tipo_tarjeta, 'monto' => $monto, 'fecha' => $fecha));
    }
    
    public function modificar_pago($critero, $nuevo) {
        $this->mongo_db->update_document('pago',$critero, $nuevo);
    }
    
    public function listar_pago($id = '') {
        if(empty($id)) {
            return $this->mongo_db->find_collection('pago');
        }
        $pagos = $this->mongo_db->find_collection('pago');
        foreach ($pagos as $pago) {
            if($pago['id'] == $id) {
                return $pago;
            }
        }
    }
}
