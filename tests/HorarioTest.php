<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HorarioTest
 *
 * @author Nitkellem
 */
class HorarioTest extends PHPUnit_Framework_TestCase{
    private $CI;

	public function setUp() {
		$this->CI = &get_instance();
	}
        public function ingresarHorario(){
                $this->CI->load->model('m_horario');
                $horario = $this->CI->m_horario->ingresar_empleado('1', '-1', '0-0-0', '0-0-0', '00:00:00');
                $this->assertTrue($horario);     
        }
        public function modificarHorario(){
                $this->CI->load->model('m_horario');
                $horario = $this->CI->m_horario->modificar_empleado('1', '-1', '0-0-0', '0-0-0', '00:00:00');
                $this->assertTrue($horario);     
        }
        public function eliminarHorario(){
                $this->CI->load->model('m_horario');
                $horario = $this->CI->m_horario->eliminar_empleado('1');
                $this->assertTrue($horario);     
        }
    //put your code here
}
