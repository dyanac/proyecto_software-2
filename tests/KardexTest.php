<?php

class KardexTest extends PHPUnit_Framework_TestCase {

    private $CI;

    public function setUp() {
        $this->CI = &get_instance();
    }
    
    public function testRegistrar_kardex() {
        $this->CI->load->model('m_kardex');
        $kardexs = $this->CI->m_kardex->registrar_kardex('-1','test','test','0','0-0-0');
        $this->assertTrue($kardexs);
    }
    
    public function testModificar_kardex() {
        $this->CI->load->model('m_kardex');
        $kardexs = $this->CI->m_kardex->update_document('kardex','-1','-1');
        $this->assertTrue($kardexs);
    }
    
    public function testEliminar_kardex() {
        $this->CI->load->model('m_kardex');
        $kardexs = $this->CI->m_kardex->eliminar_kardex('kardex','-1');
        $this->assertTrue($kardexs);
    }
    
    public function testListar_kardex() {
        $this->CI->load->model('m_kardex');
        $kardexs = $this->CI->m_kardex->listar_kardex();
        $this->assertEmpty($kardexs);
    }

}
