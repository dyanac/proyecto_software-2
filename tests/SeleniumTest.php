<?php

class SeleniumTest extends PHPUnit_Extensions_Selenium2TestCase {

    protected function setUp() {
        $this->setBrowser('firefox');
        $this->setBrowserUrl('modulo.local/');
    }

    public function testTitle() {
        $this->url('modulo.local/');
        $this->assertEquals('Welcome to CodeIgniter', $this->title());
    }

}
